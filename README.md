# Installer for GoldenEye: Source

## Summary

This is the installer for GoldenEye: Source. It can also be used for other Source mods by tweaking the [version.nsh](version.nsh).

Features include:

* Ability to choose custom install paths (not just `sourcemods`)
* Network installer capability, able to download the mod over bittorrent
* Check for and install updates (within the same minor version series, e.g. 6.0.X)
  * Updates downloaded via HTTP(S) only
* Validating integrity of downloaded files via `minisign`

## Minisign setup

This is for verifying any downloaded files prior to using them, including the update index and any update files.

First, generate a keypair using Minisign if you have not done so already, using `minisign -G`. Then, specify the output public key in [version.nsh](version.nsh).

Any downloaded files which are verified must have a corresponding `.minisig` file. To generate this signature file, use `minisign -Sm <file>`. For more info on how to use the patching system with these signature files, see the patching documentation linked in the section below.

The Minisign executable can be found in this project's `bin` folder or downloaded from the [upstream project](https://jedisct1.github.io/minisign/).

## Patching

The installer contains a minimal patching system. The full description of this patching system is quite long, so it has its own document [defining](docs/updater.md) how to use it.

Once you have the patching system figured out, simply set `UPDATE_FILE_URL` in [version.nsh](version.nsh) to the URL of your index file.

## Compiling

When building this installer, make sure to adjust all the values in [version.nsh](version.nsh), which has about everything that needs to be changed on a per-release basis.

In addition, setting up minisign and the patching system as described above is a necessity. Without it, the full installer will complain about not being able to check for updates and the other installers won't work at all.

### Network installer

Compile flags:
* NETWORK

The network installer downloads the mod over Bittorrent. The following steps are necessary to compile the network installer:

1. Create a `gesource.zip`, create and sign a .torrent file. More details on how these pieces are tied together are detailed in the [update spec](docs/updater.md).
1. Compile `installer.nsi`.

### Full installer

Compile flags:
* FULL

Due to NSIS and Windows limits on executable file sizes, the full installer is distributed with a content archive that is separate from the installer exe. To make this easier for users, we distribute this as a `.iso` file as users can double-click the file to mount it and then run its contents. The iso remains effectively read-only to protect from accidental modification, which is a nice bonus.

#### Prerequisites:

* OSCDIMG tool, which can be obtained from the [Windows ADK](https://learn.microsoft.com/en-us/windows-hardware/get-started/adk-install).
* The location of the OSCDIMG tool added to your %PATH%

Particular version shouldn't matter, but for reference, this process was tested with the Windows 11 22H2 ADK.

#### Full installer/ISO build:

1. Compile `full_installer.nsi` to generate the full installer program.
1. Create a folder to put the iso files in. Exact name doesn't matter; we'll just call this folder `iso_root`.
1. Create the folder `GoldenEye Source Files` * inside of the `iso_root` folder.
1. Put `gesource.zip` in the folder created in the previous step.
1. Create a signature and put it in the same folder, named `gesource.zip.minisig`.
1. Drag the `iso_root` folder on top of the Create ISO script. Enter the version number when prompted.
1. The ISO should be created. Mount the iso and verify its contents according to the file tree below.
1. You can delete the `iso_root` folder.

*\* Note: The folder name `GoldenEye Source Files` is generated from the ${FILE_NAME} constant defined in version.nsh. The full path the code looks for is `${FILE_NAME} Files`*

#### Finished ISO layout:

```
GoldenEye_Source_<version>_full.exe
GoldenEye Source Files
├── gesource.zip
└── gesource.zip.minisig
```

### Patch installer

Compile flags:
* PATCH

This is a lightweight installer specifically for downloading patches. This is intended to be used by the [update utility](https://gitlab.com/soupcan/ges-updater) launched by GE:S, due to its small file size.

1. Compile `installer_patch.nsi`.

### Compiler options

These options may be defined in makensis to modify installer compilation:

* `INSTALLER_BUILD_TIME`: Lets you set an installer build time string, as opposed to generating it automatically. This string is used as the installer version number and used by e.g. installer update checks. The format for this string is `YYYY.MM.DD.HHMM`. Reasons to define this manually include e.g. to set the same time for multiple installer types (e.g. full and bittorrent)

## Command Line

The installer includes some command line flags:

* `/?`: Show version and help information
* `/AutoUpdate`: Patch installer only: Skips intro page and jumps straight to installation.
* `/DisableSignatureCheck`: Disables signature checking. May be useful for testing new updates.
* `/UpdateIndex=<url>`: Specifies a custom URL for checking for updates. May be useful for testing new updates.

