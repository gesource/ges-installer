; Initialize this language
; Full language list: http://pastebin.com/raw/JyV531w9
!insertmacro LANG_LOAD English

; Init
${LangString} UNSUPT_OS "Sorry, a 64-bit version of Windows 10 or newer is required to run this installer. This program will now exit."
${LangString} STEAMERR "Steam was not found on this computer.$\r$\n$\r$\nSteam must be installed on this computer to run ${PROD_NAME}. This program will now exit."
${LangString} GAME_RUNNING "A running instance of ${PROD_NAME} was detected. If you continue, the installer will terminate the running process.$\r$\rProcess ID: $0"
${LangString} GAME_RUNNING_DECLINE "Setup will now exit."

; Cancel dialog
${LangString} ABORT "Quit ${PROD_NAME} setup?"
; Status when waiting on cancel dialog
${LangString} WAITING_ON_ABORT "Waiting for user input"

; Welcome page
${LangString} WELCOME_TITLE "${PROD_NAME} Setup"
!ifdef FULL
	${LangString} WELCOME_TEXT "This program will install ${PROD_NAME}.$\r$\nClick $(^NextBtn) to proceed with installation."
!else
	!ifdef PATCH
		${LangString} WELCOME_TEXT "This program will update ${PROD_NAME}.$\r$\nClick $(^NextBtn) to proceed with installation."
	!else
		${LangString} WELCOME_TEXT "This program will install ${PROD_NAME}.$\r$\nClick $(^NextBtn) to proceed with installation.$\r$\n$\r$\nYou are using the network installer, which will download files over a peer-to-peer protocol. Please see the license for more information."
	!endif
!endif
;; "License" button
${LangString} LICENSE "License"
; Update check
;; Title/subtitle
${LangString} UPDATECHECK_H1 "Checking for Updates"
;; Other
${LangString} NEW_INSTALLER_AVAILABLE_NETINSTALL "An updated installer is available. The updated installer is necessary for network installations."
${LangString} NEW_INSTALLER_AVAILABLE_FULL "An updated installer is available. Would you like to download it now?"
${LangString} NEW_INSTALLER_DECLINED_FULL "We can't check for updates without the latest installer.$\r$\n$\r$\n${PROD_NAME} ${PROD_VERSION} will be installed."
${LangString} UPDATE_CHECK_FAILED "Setup could not check for updates.$\r$\n$\r$\nRetry checking for updates?"
${LangString} UPDATE_CHECK_FAILED_CRITICAL "Setup could not check for updates and will now close.$\r$\n$\r$\nConsider using the full installer, which does not have a connectivity requirement."
${LangString} UPDATE_CHECK_DECLINED "Error checking for updates. ${PROD_NAME} ${PROD_VERSION} will be installed."
${LangString} UPDATE_DL_FAILED_CRITICAL "Setup failed to download updates and cannot continue. Check the log for more information."
${LangString} PATCH_INSTALLER_UPDATE_NOT_APPLICABLE "This update is not applicable to your copy of ${PROD_NAME}. Please use the full installer."
${LangString} PATCH_INSTALLER_UP_TO_DATE "${PROD_NAME} v$GES_UPDATE_VERSION is already installed."
; Install options page
;; Title/subtitle
${LangString} INSTALLOPTS_H1 "Install Options"
${LangString} INSTALLOPTS_H2 "Review the options below and then click $(^InstallBtn)."
;; Update options
${LangString} UPDATE_OPTIONS "Update options"
${LangString} INSTALL_UPDATE "Update $GES_INSTALLED_VERSION to $GES_UPDATE_VERSION"
${LangString} UPDATE_NOT_POSSIBLE "Can't update $GES_INSTALLED_VERSION to $GES_UPDATE_VERSION"
${LangString} REINSTALL "Re-install"
${LangString} KEEP_ACHIEVEMENTS "Keep achievements"
;; Installation directory
${LangString} INST_TO "Install location"
${LangString} INST_BROWSE "Change"
${LangString} INST_BROWSE_TITLE "Choose where to install ${PROD_NAME}"
${LangString} INST_DIR_FIX "The install directory must be named $\"${MODDIR}$\".$\r$\n$\r$\nDo you want to change the path to $R0?"
${LangString} INST_DIR_CHANGE_NOT_PERMITTED "Cannot change install location because ${PROD_NAME} is currently installed."
${LangString} INST_DIR_RESET_TO_DEFAULT "Reset to default"
${LangString} INST_DIR_CONFIRM_RESET "Are you sure you want to reset the installation directory?$\r$\n$\r$\nIt will be reset to:" ; newline and then display default instdir
;; Disk space
${LangString} DISK_REQ "Required:"
${LangString} DISK_FREE "Free:"
${LangString} MEGABYTE "MB"
${LangString} DISK_INSUFFICIENT_TEMP_SPACE "Insufficient space on the drive containing the temp folder$\r$\n$PLUGINSDIR$\r$\n$\r$\nAt least $GES_DOWNLOAD_SIZE$(MEGABYTE) is required at this location to download temp files."
${LangString} DISK_INSUFFICIENT_SPACE "Insufficient space on the drive containing$\r$\n$INSTDIR\$\r$\n$\r$\nContinue anyway?"
${LangString} DISK_INSUFFICIENT_SPACE2 "Insufficient space on the drive containing$\r$\n$INSTDIR$\r$\n$\r$\nFree up space on the install drive or choose another one."
${LangString} NA "N/A"
;; Start icon checkbox
${LangString} CREATE_START_ICON "Create Start menu shortcut"
;; Seed torrent checkbox
${LangString} SEED_TORRENT "Seed files during download"
;; Seed torrent messagebox
${LangString} SEED_TORRENT_MB "If this option is checked, your upload bandwidth will temporarily be used to redistribute files to other users who are either running the installer or downloading the torrent. Seeding ends once the downloads phase has completed.$\r$\n$\r$\nWe request that you keep this box checked unless you have a slow or metered connection."

; Install sections
;; Uninstall old version
${LangString} UNINSTALLING "Removing currently installed version"
;; GE:S Download/Install
${LangString} LINK_REMOVE_ERROR "Could not remove old link $SOURCEMODS\${MODDIR}"
${LangString} LINK_ERROR "Could not create link$\r$\n$SOURCEMODS\${MODDIR} -> $INSTDIR$\r$\n$\r$\n Installation cannot continue."
${LangString} DOWNLOAD_PROGRESS "Download progress:" ; shown during patch download
${LangString} DOWNLOAD_ERROR "Failed to download files necessary for installation."
${LangString} DOWNLOADING0 "Starting downloader..."
${LangString} DOWNLOADING1 "Downloading ${PROD_NAME}... This may take a while, please be patient."
${LangString} DOWNLOADING2 "Downloading ${PROD_NAME} ($1)" ;$1 is the ETA below
${LangString} DOWNLOAD_STALLED "The file is 0% downloaded. If you continue to have problems, consider downloading the Full installer."
${LangString} ETA_HOURS "$0 hours and $1 minutes remaining"
${LangString} ETA_1HOUR "1 hour and $1 minutes remaining"
${LangString} ETA_MINUTES "$1 minutes remaining"
${LangString} ETA_1MINUTE "Less than 1 minute remaining"
${LangString} VALIDATING "Verifying downloaded files"
${LangString} VALIDATE_ERROR "Could not validate signature ($0)."
${LangString} VALIDATE_ERROR_FULLINSTALLER "Failed to validate content archive. Please redownload ${PROD_NAME}."
${LangString} INSTALLING "Installing ${PROD_NAME}"
;; Patch download/install
${LangString} PATCH_DOWNLOADING "Downloading updates"
${LangString} UPDATER_DOWNLOADING "Downloading update utility"
${LangString} UPDATER_DOWNLOAD_ERROR "The optional updater utility failed to download. Retry?"
${LangString} PATCH_DOWNLOAD_ERROR "Error downloading updates. Retry?"
;; Failure
${LangString} INSTALL_FAILED "An error occured, please see the log for troubleshooting information."
;; Finish Page
${LangString} FINISH_TEXT "You must restart Steam for ${PROD_NAME} to appear in your library."
${LangString} OPEN_CHANGELOG "$GES_UPDATE_VERSION changelog"

; Uninstaller
${LangString} UN.STEAMERR "Could not find Steam path. ${PROD_NAME} will be uninstalled but may leave a dangling link in the sourcemods folder."
${LangString} UN.CHECKBOX_HEADER "Some settings are saved in case you play ${PROD_NAME} again. Checking both of these boxes will completely remove ${PROD_NAME}."
${LangString} UN.REMOVE_GAMESTATE "Remove achievements"
${LangString} UN.REMOVE_SYS_CONFIG "Remove settings"

; File version info
${VersionKey} "FileDescription" "${PROD_NAME} Setup"

; The below keys should not be localized
${VersionKey} "ProductName" "${PROD_NAME}"
${VersionKey} "CompanyName" "Team GoldenEye: Source"
${VersionKey} "FileVersion" "${INSTALLER_BUILD_TIME}"
${VersionKey} "ProductVersion" "${PROD_VERSION}"
${VersionKey} "LegalCopyright" " "
