!define CreateLink	"!insertmacro CreateSymbolicLink"
!define IsLink 		"!insertmacro IsSymbolicLink"
!define DeleteLink	"!insertmacro DeleteSymbolicLink"

!macro CreateSymbolicLink Link Target
	Push ${Target}
	Push ${Link}
	Call CreateSymbolicLink
!macroend

; Output is an outvar where 1 = link and 0 = not link
!macro IsSymbolicLink Link Output
	Push ${Link}
	Call IsSymbolicLink
	Pop ${Output}
!macroend

!macro DeleteSymbolicLink Link
	Push ${Link}
	Call DeleteSymbolicLink
!macroend

; Create the symbolic link path at the top of the stack,
; link it to the target at the 2nd index of the stack.
Function CreateSymbolicLink
	Exch $0 ; Symbolic link path
	Exch
	Exch $1 ; Symbolic link target
	Push $2 ; PowerShell exit code

	DetailPrint "Creating symbolic link: '$0' -> '$1'"

	IfFileExists $0 +1 PathDoesntExist
		DetailPrint "ERROR: Path already exists."
		SetErrors
		Goto Return
	PathDoesntExist:

	IfFileExists $1 TargetExists
		DetailPrint "ERROR: Target doesn't exist."
		SetErrors
		Goto Return
	TargetExists:

	nsExec::ExecToLog "powershell.exe -c New-Item -ItemType SymbolicLink -Path '$0' -Target '$1'"
	Pop $2

	StrCmp $2 0 ExitCodeZero
		DetailPrint "ERROR: PowerShell returned non-zero exit code $2 when creating symbolic link."
		SetErrors
		Goto Return
	ExitCodeZero:

	IfFileExists $0 PathExistsAfterCreation
		DetailPrint "ERROR: Path doesn't exist after creation."
		SetErrors
		Goto Return
	PathExistsAfterCreation:

	Return:

	Pop $2
	Pop $1
	Pop $0

FunctionEnd

; Check if the file/folder path at the top of the stack is a symbolic link.
; It replaces the top item of the stack with 1 (true) or 0 (false)
Function IsSymbolicLink
	Exch $0 ; Symbolic link path, and then 1 or 0
	Push $1 ; PowerShell output / exit code

	DetailPrint "Checking if $0 is a symbolic link"

	nsExec::ExecToStack "powershell.exe -c Write-Output (Get-Item '$0').LinkType"
	Pop $1

	StrCmp $1 0 SuccessExitCode
		DetailPrint "PowerShell returned non-zero exit code $1"
		SetErrors
		Goto Return
	SuccessExitCode:

	Pop $1
	${Trim} $1 $1

	; Default value
	StrCpy $0 0

	; Set to 1 if symbolic link
	StrCmp $1 "SymbolicLink" +1 Return
		StrCpy $0 1

	Return:

	Pop $1
	Exch $0
FunctionEnd

; Delete the symbolic link path at the top of the stack
Function DeleteSymbolicLink
	Exch $0 ; Symbolic link to delete
	Push $1 ; Misc scratch var

	DetailPrint "Deleting symbolic link: '$0'"

	${IsLink} $0 $1
	StrCmp $1 1 IsALink
		DetailPrint "ERROR: Path '$0' is not a symbolic link."
		SetErrors
		Goto Return
	IsALink:

	nsExec::ExecToLog "powershell.exe -c (Get-Item '$0').Delete()"
	Pop $1

	StrCmp $1 0 DeletedSuccessfully
		DetailPrint "ERROR: PowerShell returned non-zero exit code $1 when deleting symbolic link."
		SetErrors
		Goto Return
	DeletedSuccessfully:

	IfFileExists $0 +1 DeletedSuccessfully2
		DetailPrint "ERROR: Link path $0 exists after deletion."
		SetErrors
		Goto Return
	DeletedSuccessfully2:

	Return:

	Pop $1
	Pop $0
FunctionEnd