; Header file with functions for starting, checking status, and stopping NSIS downloads.

; Functions:
; GetTransmissionDownloadStatus: Queries DL status and updates variables (described below)

Var TransmissionSessionId
Var TRANSMISSION_PERCENT_COMPLETE ; Download completion %
Var TRANSMISSION_BYTES_LEFT ; Bytes until download completion. '0' if completed.
Var TRANSMISSION_TIME_LEFT_SECONDS ; Time, in seconds, until download will complete.
Var TRANSMISSION_TIME_LEFT_STR ; Time left, in human-readable format
Var RPC_PORT ; RPC port of transmission-daemon
Var PEER_PORT ; peer port of transmission-daemon

!ifdef DEBUG
  !define _TMD_DetailPrint "DetailPrint"
!else
  !define _TMD_DetailPrint ";"
!endif

Function PrimitiveJsonDecode

  ; This function searches for a string, then returns the text that follows that string, up until a comma
  ; Strings are then trimmed, and if there are double quotes on both sides of the string, they are removed.

  ; Like I said... primitive.

  ; To use, push the input string (the haystack), and then push the search string (the needle)
  ; Call the function, and the resulting string will be on the top of the stack.

  ; Push '{ my json... }'
  ; Push '"search string":'
  ; Pop $resultVar

  ; I made this because NSJson doesn't seem to be able to parse a JSON string from memory...
  ; I would have to write, read, and delete JSON files every time I checked the download status.
  ; Quite wasteful!

  Exch $6 ; Search string (needle)
  Exch
  Exch $0  ; Input string (haystack)
  Push $1 ; Accumulator
  Push $2 ; Length of input string. Error out if loop iterations exceed this.
  Push $3 ; String currently being compared during lookup
  Push $4 ; Output string
  Push $5 ; Length of search string
  Push $7 ; Secondary accumulator for LookForValue
          ; This one tracks the length of the value

  StrCpy $1 0
  StrLen $2 $0
  StrLen $5 $6

  ${_TMD_DetailPrint} "Input string: $0"
  ${_TMD_DetailPrint} "Search string: $6"

  LookForString:
    IntCmp $2 $1 Error
    StrCpy $3 $0 $5 $1
    ${_TMD_DetailPrint} "Comparing string $3"
    IntOp $1 $1 + 1
    StrCmp $3 $6 0 LookForString
    ${_TMD_DetailPrint} "Found the string $\"$3$\" at position $1"
    Goto GotString

  Error:
    MessageBox MB_OK "An error occurred parsing for search string $6."
    DetailPrint "Failed to find $6"
    DetailPrint "Input data: $0"
    Abort

  GotString:

  IntOp $1 $1 + $5 ; Advance input str index to the first char after our search string.
  StrCpy $7 1

  ; Now get everything leading up to the next ',' or EOL
  LookForValue:
    IntCmp $2 $1 GotValue ; Assume end of value if end of string
    StrCpy $3 $0 1 $1 ; Store current char for comparison purposes
    ${_TMD_DetailPrint} "Comparing potential value terminator $3"
    StrCmp $3 "," GotValue
    StrCmp $3 "}" GotValue
    IntOp $1 $1 + 1 ; Increment input str index
    IntOp $7 $7 + 1 ; Increment value str index
    Goto LookForValue

  GotValue:

  IntOp $1 $1 - $7 ; Rewind the input str index by the value len
  StrCpy $4 $0 $7 $1

  ${_TMD_DetailPrint} "Value length: $7"
  ${_TMD_DetailPrint} "Value index: $1"

  ${_TMD_DetailPrint} "Got value: '$4'"

  ; Variable assignments other than $4 and $7 don't mean anything after this point
  ${Trim} $4 $4

  ; If the value has quotes on both sides, remove them.
  ; $0 = first char
  ; $1 = last char
  StrCpy $0 $4 1
  StrCpy $1 $4 1024 -1
  StrCmp $0 '"' +1 DontTrimQuotes
  StrCmp $1 '"' +1 DontTrimQuotes

  ; After trimming the quotes, the length of the string is 2 chars less
  IntOp $7 $7 - 2

  StrCpy $4 $4 1024 1
  StrCpy $4 $4 $7

  DontTrimQuotes:

  ; Copy the outvar to $6 so we can exch it later
  StrCpy $6 $4

  Pop $7
  Pop $5
  Pop $4
  Pop $3
  Pop $2
  Pop $1
  Pop $0
  Exch $6
FunctionEnd

Function GetTransmissionSessionId
  ; This function gets the Session ID necessary to talk to Transmission-Daemon

  Push $0
  Push $1
  Push $2
  Push $3

  DetailPrint "Getting Session ID from Transmission daemon"
  NSxfer::Transfer /MODE SILENT /URL "http://127.0.0.1:$RPC_PORT/transmission/rpc/" /METHOD "GET" /LOCAL "MEMORY" /RETURNID /END
  Pop $0
  
  ${_TMD_DetailPrint} "Transfer ID: $0"

  NSxfer::Query /ID $0 /RECVHEADERS /END
  Pop $0

  ${_TMD_DetailPrint} "Headers: $0"

  ; Get the transmission session ID from output string

  ; $0 is the raw headers
  ; $1 is the accumulator used for our loops
  ; $2 is the string length of the raw headers
  ; $3 is the string used for search and string comparison

  StrCpy $1 0
  StrLen $2 $0

  LookForSessionId:
    IntCmp $2 $1 Error
    StrCpy $3 $0 27 $1
    ${_TMD_DetailPrint} "Comparing string $3"
    IntOp $1 $1 + 1
    StrCmp $3 "X-Transmission-Session-Id: " 0 LookForSessionID
    ${_TMD_DetailPrint} 'Found the string $\"$3$\"'
    IntOp $1 $1 + 26
    StrCpy $TransmissionSessionId $0 48 $1
    DetailPrint "Session ID: $\"$TransmissionSessionId$\""
    Goto GotSessionId

  Error:
    MessageBox MB_OK "An error occurred reading the Transmission session ID."
    DetailPrint "Response data: $0"
    Abort

  GotSessionId:

  Pop $3
  Pop $2
  Pop $1
  Pop $0
FunctionEnd

Function GetTransmissionDownloadStatus
  Push $0
  Push $1
  Push $2
  Push $3
  Push $4

  ; Get transmission session ID if it hasn't been set yet
  StrCmp $TransmissionSessionId "" +1 +2
    Call GetTransmissionSessionId

  StrCpy $0 '\
  {\
    "arguments": {\
      "fields": ["eta", "percentDone", "rateDownload", "leftUntilDone"]\
    },\
    "method": "torrent-get"\
  }\
    '

  GetStatus:

  NSxfer::Transfer /MODE SILENT /URL "http://127.0.0.1:$RPC_PORT/transmission/rpc/"\
    /METHOD POST /DATA "$0" /HEADERS "X-Transmission-Session-Id: $TransmissionSessionId"\
    /LOCAL "MEMORY" /RETURNID /END
  Pop $1

  NSxfer::Query /ID $1 /ERRORCODE /CONTENT /ERRORTEXT /END
  Pop $2
  Pop $3
  Pop $4

  StrCmp $2 409 +1 ValidSessionId
    Call GetTransmissionSessionId
    Goto GetStatus

  ValidSessionId:

  StrCmp $2 200 HTTPOK
    DetailPrint "WinINET returned unhandled HTTP status $2"
    DetailPrint "Request ID: $1"
    DetailPrint "POST content: $0"
    DetailPrint "Transmission-Daemon responded: $3"
    DetailPrint "Error text: $4"
    MessageBox MB_OK|MB_ICONSTOP "$(INSTALL_FAILED)"
    Abort

  HTTPOK:

  ; Remove the transfer from the queue.
  ; Queries will eventually fail if the queue gets full.
  NSxfer::Set /ID $1 /REMOVE /END
  Pop $1 ; We don't care what this var is, we just pop it off the stack.

  ${_TMD_DetailPrint} "Response Code:$\r$\n$2"
  ${_TMD_DetailPrint} "Data:$\r$\n$3"

  Push $3
  Push '"leftUntilDone":' ; "true" or "false"
  Call PrimitiveJsonDecode
  Pop $TRANSMISSION_BYTES_LEFT

  Push $3
  Push '"percentDone":'
  Call PrimitiveJsonDecode
  Pop $TRANSMISSION_PERCENT_COMPLETE

  ; The output of percentDone is a float with 4 decimal points.
  ; Turn it into a whole number.
  StrCpy $TRANSMISSION_PERCENT_COMPLETE $TRANSMISSION_PERCENT_COMPLETE 2 2

  ; Also, if there's a leading zero, delete it.
  StrCpy $0 $TRANSMISSION_PERCENT_COMPLETE 1
  StrCmp $0 0 +1 DontDeleteZero
    StrCpy $TRANSMISSION_PERCENT_COMPLETE $TRANSMISSION_PERCENT_COMPLETE 1 1
  DontDeleteZero:

  Push $3
  Push '"eta":'
  Call PrimitiveJsonDecode
  Pop $TRANSMISSION_TIME_LEFT_SECONDS

  ${Validate} $0 $TRANSMISSION_TIME_LEFT_SECONDS ${NUMERIC}
  StrCmp $0 1 ValidTime
    StrCpy $TRANSMISSION_TIME_LEFT_STR '???'
    Goto TheEndOfTime

  ValidTime:

  IntOp $2 $TRANSMISSION_TIME_LEFT_SECONDS / 60
  IntOp $0 $2 / 60 ; Hours
  IntOp $1 $2 % 60 ; Minutes after subtracting whole hours

  IntCmp $TRANSMISSION_TIME_LEFT_SECONDS 3600 0 LessThanOneHour

    IntCmp $0 1 0 0 PluralHours
      StrCpy $TRANSMISSION_TIME_LEFT_STR "$(ETA_1HOUR)"
      Goto TheEndOfTime

    PluralHours:
      StrCpy $TRANSMISSION_TIME_LEFT_STR "$(ETA_HOURS)"
      Goto TheEndOfTime

  LessThanOneHour:

  IntCmp $TRANSMISSION_TIME_LEFT_SECONDS 60 0 LessThanOneMinute
    StrCpy $TRANSMISSION_TIME_LEFT_STR "$(ETA_MINUTES)"
    Goto TheEndOfTime

  LessThanOneMinute:

  StrCpy $TRANSMISSION_TIME_LEFT_STR "$(ETA_1MINUTE)"

  TheEndOfTime:

  Pop $4
  Pop $3
  Pop $2
  Pop $1
  Pop $0
FunctionEnd