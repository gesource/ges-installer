; Pages
UninstPage custom un.welcomeShow un.welcomeLeave
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH

; This just makes sure a couple MUI language strings we want get defined:
; MUI_UNTEXT_CONFIRM_TITLE and MUI_UNTEXT_CONFIRM_SUBTITLE
; otherwise, it serves no purpose.
!define MUI_UNCONFIRMPAGE

; Uninstaller-specific variables
;; General variables
Var un_ConfigRestDir
Var un_ExePath
Var un_progressbar
;; nsDialogs controls
Var un_chkRemoveGamestate
Var un_chkRemoveSysConfig

Function un.onInit
  ; make sure this is ready for later use in the installer
  InitPluginsDir

  ; Get uninstaller path
  ReadRegStr $un_ExePath HKLM32 "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MODDIR}" "UninstallString"

  ; Get sourcemods path
  ; Autoredirection for 32-bit programs will take care of Wow6432Node for us
  ReadRegStr $0 HKLM32 "Software\Valve\Steam" "InstallPath"

  ; Verify string is not empty
  StrCmp $0 "" Steam_Error +1

  ; Verify location exists
  IfFileExists "$0\*.*" +1 Steam_Error

  ; Set sourcemods path, clean up, finish Steam section
  StrCpy "$SOURCEMODS" "$0\steamapps\sourcemods"
  StrCpy $0 ""
  Goto Steam_Done

  Steam_Error:
    MessageBox MB_OK|MB_ICONINFORMATION $(UN.STEAMERR)
    StrCpy $SOURCEMODS ""
  Steam_Done:
  ; Sourcemods path end

  ; Get INSTDIR
  ReadRegStr $INSTDIR HKLM32 "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MODDIR}" "InstallLocation"

  ; Set un_ConfigRestDir, which is the directory to restore configs to.
  ; Default: set $un_ConfigRestDir = $SOURCEMODS\${MODDIR}
  StrCpy $un_ConfigRestDir "$SOURCEMODS\${MODDIR}"
  ; If sourcemods isn't set, set $0 = $INSTDIR instead
  ; (if sourcemods couldn't be found, at least restore it to the instdir if
  ; nothing else)
  StrCmp $SOURCEMODS "" +1 sourcemodsSet
    StrCpy $un_ConfigRestDir $INSTDIR
  sourcemodsSet:
FunctionEnd

;;; Install sections

; RMDir /r doesn't update the progress bar, so we show a marquee progress bar
Section "un." "UN.PROGRESSBAR"
  ; Get HWND of the progress bar
  FindWindow $un_progressbar "#32770" "" $HWNDPARENT
  GetDlgItem $un_progressbar $un_progressbar 1004

  ; Set our custom progress bar to marquee
  System::Call "user32::GetWindowLong(i $un_progressbar, i ${GWL_STYLE}) i .r1"
  System::Call "user32::SetWindowLong(i $un_progressbar, i ${GWL_STYLE}, i $1|${PBS_MARQUEE})"
  SendMessage $un_progressbar ${PBM_SETMARQUEE} 1 33
SectionEnd

Section "un." "UN.SHORTCUT"
  SetShellVarContext all
  Delete "$SMPROGRAMS\${FILE_NAME}.url"
  DeleteINIStr "$SMPROGRAMS\desktop.ini" "LocalizedFileNames" "${FILE_NAME}.url"
SectionEnd

Section "un." "UN.GES_DEL_UNINST_DATA_IF_GES_NOT_PRESENT"
  ; If $INSTDIR is empty or doesn't exist, simply delete uninstaller + uninstall
  ; data, and quit.
  Goto check

  delInstData:
    ; Delete uninstaller
    Delete $un_ExePath
    ; Delete uninstall data
    DeleteRegKey HKLM32 "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MODDIR}"
    Quit

  check:
  ; Check that instdir is not empty
  StrCmp "$INSTDIR" "" delInstData +1
  ; check that instdir exists
  IfFileExists "$INSTDIR\*.*" +1 delInstData
SectionEnd

Section "un." "UN.BACKUP_ACHIEVEMENTS"
  ; Backup achievements/weapon skins/etc
  ; Restored in a later section, if enabled
  CreateDirectory "$PLUGINSDIR\BACKUP"
  CopyFiles "$INSTDIR\gamestate.txt" "$PLUGINSDIR\BACKUP\gamestate.txt"
  CopyFiles "$INSTDIR\gesdata.txt" "$PLUGINSDIR\BACKUP\gesdata.txt"
SectionEnd

Section "un." "UN.BACKUP_CONFIG"
  ; Backup cfg\config.cfg
  ; Restored in a later section, if enabled
  CreateDirectory "$PLUGINSDIR\BACKUP"
  CopyFiles "$INSTDIR\cfg\config.cfg" "$PLUGINSDIR\BACKUP\config.cfg"
SectionEnd

Section /o "un." "UN.DELETE_REGISTRY_DATA"
  ; Loop through users' registries and delete GE:S settings
  StrCpy $0 0 ; Accumulator
  ; $1 = the current enumerated value

  loop:
    EnumRegKey $1 HKEY_USERS "" $0
    StrCmp $1 "" fin +1

    ; If instdir var not empty,
    ; delete reg key assuming there is a settings key for the instdir
    ; even if this is different from sourcemods, this could theoretically exist
    ; depending on how the user launched the mod
    StrCmp $INSTDIR "" no_instdir +1
    DeleteRegKey HKEY_USERS "$1\Software\Valve\Source\$INSTDIR"
    no_instdir:

    ; if sourcemods var not empty, delete reg key
    StrCmp $SOURCEMODS "" no_sourcemods +1
    DeleteRegKey HKEY_USERS "$1\Software\Valve\Source\$SOURCEMODS\${MODDIR}"
    no_sourcemods:

    IntOp $0 $0 + 1
    Goto loop

  fin:
SectionEnd

Section "un." "UN.DELETE_GES"
  ; Delete GE:S
  RMDir /r "$INSTDIR"

  ; Delete $SOURCEMODS\${MODDIR} - removes symlink if there is one
  StrCmp $SOURCEMODS "" no_sourcemods +1
    RMDir /r "$SOURCEMODS\${MODDIR}"
  no_sourcemods:

  ; Delete updater
  Delete "$SOURCEMODS\${MODDIR}_update.exe"
  ; Delete uninstaller
  Delete $un_ExePath
  ; Delete uninstall data
  DeleteRegKey HKLM32 "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MODDIR}"
SectionEnd

Section "un." "UN.RESTORE_ACHIEVEMENTS"
  CreateDirectory $un_ConfigRestDir
  CopyFiles "$PLUGINSDIR\BACKUP\gamestate.txt" "$un_ConfigRestDir\gamestate.txt"
  CopyFiles "$PLUGINSDIR\BACKUP\gesdata.txt" "$un_ConfigRestDir\gesdata.txt"
SectionEnd

Section "un." "UN.RESTORE_CONFIG"
  CreateDirectory "$un_ConfigRestDir\cfg"
  CopyFiles "$PLUGINSDIR\BACKUP\config.cfg" "$un_ConfigRestDir\cfg\config.cfg"
SectionEnd

;;; Install sections end

Function un.welcomeShow
  ; Welcome page
  ; This is basically a remake of the standard welcome page,
  ; except that it adds a couple of GE:S specific options

  !insertmacro MUI_HEADER_TEXT $(MUI_UNTEXT_CONFIRM_TITLE) $(MUI_UNTEXT_CONFIRM_SUBTITLE)

  ; Hide our "License" button, which is not used by the installer.
  GetDlgItem $0 $HWNDPARENT 4
  ShowWindow $0 0

  nsDialogs::Create 1018

    ; Standard confirm page

    ${NSD_CreateLabel} 0 0 100% 24u "$(^UninstallingText)"
    Pop $0

    ${NSD_CreateLabel} 0 40u 64u 12u "$(^UninstallingSubText)"
    Pop $0

    ${NSD_CreateText} 64u 39u 236u 12u $INSTDIR
    Pop $0
    SendMessage $0 ${EM_SETREADONLY} 1 0

    ; GE:S specific options

    ${NSD_CreateLabel} 0 92u 100% 24u "$(UN.CHECKBOX_HEADER)"
    Pop $0

    ${NSD_CreateCheckBox} 0 116u 100% 12u "$(UN.REMOVE_GAMESTATE)"
    Pop $un_chkRemoveGamestate

    ${NSD_CreateCheckBox} 0 128u 100% 12u "$(UN.REMOVE_SYS_CONFIG)"
    Pop $un_chkRemoveSysConfig

  nsDialogs::Show

FunctionEnd

Function un.welcomeLeave
  ; SetSectionFlags based on user-defined settings

  ; This Page code isn't executed when running the uninstaller silently.
  ; Sections will use their default flags when run silently.

  ; This is the desired behavior: we don't want to remove user data when
  ; (re)installing.

  ; Gather data
  ${NSD_GetState} $un_chkRemoveGamestate $0
  ${NSD_GetState} $un_chkRemoveSysConfig $1

  ; If user checked box to remove achievements,
  ; disable the sections to backup + restore them
  StrCmp $0 "${BST_CHECKED}" +1 skipAchievementsFlag
    SectionSetFlags ${UN.BACKUP_ACHIEVEMENTS} 0
    SectionSetFlags ${UN.RESTORE_ACHIEVEMENTS} 0
  skipAchievementsFlag:

  ; If user checked box to remove config data,
  ; disable the sections to backup/restore them
  ; also enable section to remove registry data
  StrCmp $1 "${BST_CHECKED}" +1 skipSettingsFlag
    SectionSetFlags ${UN.BACKUP_CONFIG} 0
    SectionSetFlags ${UN.RESTORE_CONFIG} 0
    SectionSetFlags ${UN.DELETE_REGISTRY_DATA} 1
  skipSettingsFlag:

FunctionEnd
