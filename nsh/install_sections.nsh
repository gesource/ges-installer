; This function is called if the install fails, or the Abort command is used
Function .onInstFailed
  !ifndef PATCH
    Call RestoreData
  !endif
  Call SaveDetailsView
  ${Status} $(INSTALL_FAILED)
FunctionEnd

Section "" INSTALL_BEGAN
  StrCpy $INSTALL_BEGAN 1

  ; Get HWND of our main progress bar
  FindWindow $prgInstfiles "#32770" "" $HWNDPARENT
  GetDlgItem $prgInstfiles $prgInstfiles 1004

  ; Get HWND of our custom progress bar
  FindWindow $prgInstfilesCustom "#32770" "" $HWNDPARENT
  GetDlgItem $prgInstfilesCustom $prgInstfilesCustom 1005

  ; Set our progress bar range from 0 - 100
  SendMessage $prgInstfilesCustom ${PBM_SETRANGE32} 0 100
SectionEnd

; download sections come before any modifications to the installed game
; this way users can cancel the download without leaving GE:S in a partially
; installed state.

; torrent download: don't include in full or patch installer;
; even if we can toggle sections this keeps it from including unnecessary files
!ifdef NETWORK

  ; function called on DL completion as well as user abort
  Function DeleteTempFirewallException
    DetailPrint "Removing temporary Windows Firewall exception"
    nsExec::ExecToLog 'netsh advfirewall firewall delete rule name="${MODDIR}_netinstall"'
    Pop $0
  FunctionEnd

  Section "" GES_DOWNLOAD
    dotTorrentDl:
    DetailPrint "Downloading torrent file"

    !insertmacro download "$GES_TORRENT" "$PLUGINSDIR\${MODDIR}.torrent" $0 "/MODE SILENT"
    !insertmacro download "$GES_TORRENT.minisig" "$PLUGINSDIR\${MODDIR}.torrent.minisig" $0 "/MODE SILENT"

    ClearErrors
    !insertmacro verifySignature "$PLUGINSDIR\${MODDIR}.torrent"
    Pop $0

    StrCmp $0 0 dotTorrentDlSuccess

    ; Prompt the user to retry or cancel. If the user chooses to cancel, abort the install
    MessageBox MB_RETRYCANCEL|MB_ICONEXCLAMATION $(DOWNLOAD_ERROR) IDRETRY dotTorrentDl
    Abort

    dotTorrentDlSuccess:

    ; Set GE:S download temp folder
    StrCpy $DOWNLOAD_FOLDER "$PLUGINSDIR\DOWNLOAD"
    ; Extract Transmission
    CreateDirectory "$PLUGINSDIR\transmission"
    SetOutPath "$PLUGINSDIR\transmission"
    File /r "bin\transmission\*"

    ; "Starting downloader..."
    ${Status} "$(DOWNLOADING0)"

    ; Initialize ports to default values --
    ; they will be incremented if unavailable
    StrCpy $RPC_PORT 9100
    StrCpy $PEER_PORT 5410

    rpcPortCheck:

    ; Check if default RPC port is free.
    ; If not, increment the port until we find one that is.
    DetailPrint "Checking if port $RPC_PORT is free."
    nsExec::Exec 'powershell.exe -c "exit (Get-NetTCPConnection -LocalPort $RPC_PORT | measure).Count"'
    Pop $0
    DetailPrint "Returned $0"

    ; If exit code is 0 (not found), proceed.
    ; Otherwise, increment port and try again.
    StrCmp $0 0 peerPortCheck
      IntOp $RPC_PORT $RPC_PORT + 10
      Goto rpcPortCheck

    peerPortCheck:

    ; Check if default peer port is free.
    ; If not, increment the port until we find one that is.
    DetailPrint "Checking if port $PEER_PORT is free."
    nsExec::Exec 'powershell.exe -c "exit (Get-NetTCPConnection -LocalPort $PEER_PORT | measure).Count"'
    Pop $0
    DetailPrint "Returned $0"

    ; If exit code is 0 (not found), proceed.
    ; Otherwise, increment port and try again.
    StrCmp $0 0 transmissionDownload
      IntOp $PEER_PORT $PEER_PORT + 10
      Goto peerPortCheck

    transmissionDownload:

    DetailPrint "Adding temporary Windows Firewall exception"

    ; Inbound

    nsExec::ExecToLog 'netsh advfirewall firewall add rule \
      name="${MODDIR}_netinstall" dir=in action=allow \
      program="$PLUGINSDIR_LONG\transmission\transmission-daemon-ges.exe" \
      enable=yes profile=any protocol=tcp localport=$PEER_PORT \
      description="This is a temporary exception for the ${FILE_NAME} installer. \
      and can safely be deleted after it is installed."'

    ; Outbound

    nsExec::ExecToLog 'netsh advfirewall firewall add rule \
      name="${MODDIR}_netinstall" dir=out action=allow \
      program="$PLUGINSDIR_LONG\transmission\transmission-daemon-ges.exe" \
      enable=yes profile=any \
      description="This is a temporary exception for the ${FILE_NAME} installer \
      and can safely be deleted after it is installed."'

    DetailPrint "Using RPC port $RPC_PORT and peer port $PEER_PORT"

    DetailPrint "Starting Transmission daemon"

    nsExec::ExecToLog 'powershell.exe -c Start-Process -WindowStyle Hidden \
      "$PLUGINSDIR\transmission\transmission-daemon-ges.exe" $\'\
      --foreground --config-dir "$PLUGINSDIR\transmission\config" \
      --no-watch-dir --no-incomplete-dir --no-portmap --no-global-seedratio \
      --pid-file "$PLUGINSDIR\transmission\daemon.pid" \
      --logfile "$PLUGINSDIR\transmission\daemon.log" \
      --rpc-bind-address 127.0.0.1 --allowed 127.0.0.1 \
      --port $RPC_PORT --peerport $PEER_PORT \
      $\''
    Pop $0

    GetPID: ; wait until PID file exists before getting PID
    Sleep 1000
    IfFileExists "$PLUGINSDIR\transmission\daemon.pid" +1 GetPID

    FileOpen $0 "$PLUGINSDIR\transmission\daemon.pid" r
    FileRead $0 $DAEMON_PID
    FileClose $0

    ${Trim} $DAEMON_PID $DAEMON_PID ; trim whitespaces off PID

    DetailPrint "Got daemon PID of $DAEMON_PID"

    DetailPrint "Signaling Transmission daemon to start download."

    ; Need to replace \ with \\ for JSON
    ${StrRep} $1 "$PLUGINSDIR\${MODDIR}.torrent" "\" "\\"
    ${StrRep} $2 "$DOWNLOAD_FOLDER" "\" "\\"

    StrCpy $0 '{"arguments":{"download-dir":"$2","filename":"$1"},"method":"torrent-add"}'
    DetailPrint "POSTing: $0"

    Call GetTransmissionSessionId

    ; RPC call to add torrent
    NSxfer::Transfer /MODE SILENT /URL "http://127.0.0.1:$RPC_PORT/transmission/rpc/"\
      /METHOD POST /DATA "$0" /HEADERS "X-Transmission-Session-Id: $TransmissionSessionId"\
      /LOCAL "MEMORY" /RETURNID /END
    Pop $0

    NSxfer::Query /ID $0 /ERRORCODE /CONTENT /END
    Pop $0
    Pop $1

    DetailPrint "WinINET responded: $0"
    DetailPrint "RPC response: $1"

    ; Set upload limit based on checkbox on install options screen
    StrCmp $chkSeedTorrent_Checked "1" 0 Checked

      StrCpy $0 '{"arguments":{"speed-limit-up":1,"speed-limit-up-enabled":true},"method":"session-set"}'
      NSxfer::Transfer /MODE SILENT /URL "http://127.0.0.1:$RPC_PORT/transmission/rpc/"\
        /METHOD POST /DATA "$0" /HEADERS "X-Transmission-Session-Id: $TransmissionSessionId"\
        /LOCAL "MEMORY" /END
      Pop $0

    Checked:

    ${Status} "$(DOWNLOADING1)"

    ; Hide main progress bar
    ShowWindow $prgInstfiles 0
    ; Show our custom one
    ShowWindow $prgInstfilesCustom 1

    StrCpy $3 0 ; Loop iteration counter
    waitForDownload:
    Sleep 1000

    Call GetTransmissionDownloadStatus
    IntCmp $TRANSMISSION_PERCENT_COMPLETE 0 SkipMsgUpdate ; Don't update if 0%
      ; These vars are defined for the localization string below
      StrCpy $0 $TRANSMISSION_PERCENT_COMPLETE
      StrCpy $1 $TRANSMISSION_TIME_LEFT_STR
      SetDetailsPrint textonly
      DetailPrint "$(DOWNLOADING2)"
      SetDetailsPrint listonly
    SkipMsgUpdate:

    ; Store percentage for comparison later
    StrCpy $2 $0
    ; Iterate accumulator
    IntOp $3 $3 + 1

    ; Show message box if the download hasn't started after a long time
    IntCmp $3 90 0 SkipDlStalled SkipDlStalled
      IntCmp $2 0 0 0 SkipDlStalled
        MessageBox MB_OK "$(DOWNLOAD_STALLED)"
    SkipDlStalled:

    ; Update progress bar
    SendMessage $prgInstfilesCustom ${PBM_SETPOS} $TRANSMISSION_PERCENT_COMPLETE 0

    ; We want users to be able to cancel while waiting on the download,
    ; so enable the cancel button.
    GetDlgItem $1 $HWNDPARENT 2
    EnableWindow $1 1

    ; If download isn't complete, loop back to waitForDownload
    StrCmp $TRANSMISSION_BYTES_LEFT 0 +1 waitForDownload

    DetailPrint "Transmission done downloading, sending the exit signal..."

    nsExec::ExecToLog 'taskkill /PID $DAEMON_PID'
    Pop $0

    ; END SECTION STUFF!
    ; DON'T PUT ANY INSTALL-RELATED COMMANDS BELOW HERE!

    ; Download finished -- disable the Cancel button
    GetDlgItem $0 $HWNDPARENT 2
    EnableWindow $0 0

    ; Hide our custom progress bar
    ShowWindow $prgInstfilesCustom 0
    ; Show the main/default one
    ShowWindow $prgInstfiles 1

    Call DeleteTempFirewallException

    ; If abort dialog is open, don't proceed to next section
    !insertmacro WAIT_ON_ABORT
  SectionEnd
!endif

Section "" PATCH_DOWNLOAD
  ; Print out update info in log
  DetailPrint "Update version: $GES_UPDATE_VERSION"
  DetailPrint "Update URL: $GES_UPDATE_URL"

  Goto Download

  PatchDlError: ; a jump point to goto if the download section encounter errors
  
  ; If this is an install using the full installer, allow the install to proceed without the update.
  ; If this is an update operation, fail the install without executing any other sections
  StrCmp $INSTALL_OR_UPDATE 'INSTALL' Install
      MessageBox MB_OK $(UPDATE_DL_FAILED_CRITICAL)
      Abort
  
  Install:

  Call clearUpdateInfo ; in case there's a network patch that failed,
                       ; clear version info

  Goto EndSection ; carry on installation in another section

  Download:
  ; Set status message
  ${Status} $(PATCH_DOWNLOADING)

  http:
    DetailPrint "Executing http download..."
    ; Download patch file
    !insertmacro download "$GES_UPDATE_URL" "$PLUGINSDIR\PATCH.zip" $0 "/ABORT '' '' /STATUSTEXT $(DOWNLOAD_PROGRESS) ''"
    ; Download signature
    !insertmacro download "$GES_UPDATE_URL.minisig" "$PLUGINSDIR\PATCH.zip.minisig" $0 "/MODE SILENT"

    DetailPrint "Validating signature"

    ; Validate signature, if enabled
    ClearErrors
    !insertmacro verifySignature "$PLUGINSDIR\PATCH.zip"

    IfErrors +1 HttpZipSum_Success
    ; If sig check failed
    ; YES = RETRY DOWNLOAD, NO = IGNORE
    MessageBox MB_YESNO "$(PATCH_DOWNLOAD_ERROR)" IDYES http IDNO PatchDlError
    HttpZipSum_Success:

    !insertmacro unzip "$PLUGINSDIR\PATCH.zip" "$PLUGINSDIR\PATCH"
    Delete "$PLUGINSDIR\PATCH.ZIP"

  ; End section
  EndSection:

SectionEnd

!ifdef FULL
Section "" VALIDATE_LOCAL_ARCHIVE
  ; full installer: validate content archive before install
  ${Status} $(VALIDATING)
  ClearErrors
  !insertmacro verifySignature "$EXEDIR\${FILE_NAME} Files\${INSTALL_ZIP_NAME}"

  IfErrors +1 NoErrors
    MessageBox MB_OK|MB_ICONSTOP $(VALIDATE_ERROR_FULLINSTALLER)
    ${Failure}

  NoErrors:
SectionEnd
!endif

!ifndef PATCH
  Section "" BACKUP_ACHIEVEMENTS
    CreateDirectory "$PLUGINSDIR\BACKUP"
    CopyFiles "$SOURCEMODS\${MODDIR}\gamestate.txt" "$PLUGINSDIR\BACKUP\gamestate.txt"
    CopyFiles "$SOURCEMODS\${MODDIR}\gesdata.txt" "$PLUGINSDIR\BACKUP\gesdata.txt"
  SectionEnd

  Section "" BACKUP_CONFIG
    CreateDirectory "$PLUGINSDIR\BACKUP"
    CopyFiles "$SOURCEMODS\${MODDIR}\cfg\config.cfg" "$PLUGINSDIR\BACKUP\config.cfg"
  SectionEnd

  Section "" UNINSTALL_OLD_VER
    ${Status} $(UNINSTALLING)
    ; Get installer location
    ReadRegStr $0 HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MODDIR}" "UninstallString"
    ; Get gesource location
    ReadRegStr $1 HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MODDIR}" "InstallLocation"
    ; Execute old uninstaller
    ExecWait '"$0" /S _?=$1'
    ; Delete uninstaller (it can't do it itself because we disable forking)
    Delete $0
    ; Delete any remaining files
    IfFileExists "$1\*.*" +1 no_delete
      RMDir /r $1
    no_delete:
  SectionEnd

  Section "" CREATE_INSTDIR
    ; if file/dir doesn't exist, go to create it
    IfFileExists "$SOURCEMODS\${MODDIR}" +1 Create

    ; Check if existing file/dir is a link, and if so, delete the link
    ; This prevents unintended data loss
    ; 1 = link, 0 = not link
    ${IsLink} "$SOURCEMODS\${MODDIR}" $0
    StrCmp $0 0 notlink +1
      ClearErrors
      ${DeleteLink} "$SOURCEMODS\${MODDIR}"
      IfErrors +1 notlink ; Goto notlink if link was deleted successfully.
      ; unsuccessful
      MessageBox MB_OK|MB_ICONEXCLAMATION $(LINK_REMOVE_ERROR)
      ${Failure}
    notlink:

    RMDir /r "$SOURCEMODS\${MODDIR}"

    Create:
    ClearErrors
    ; With all checking and error handling done, create instdir + symlink
    CreateDirectory $INSTDIR ; install directory
    ; Create symlink, necessary because Steam always looks in $SOURCEMODS
    ; Skip if instdir the same as sourcemods\moddir
    StrCmp $INSTDIR "$SOURCEMODS\${MODDIR}" success +1
    ${CreateLink} "$SOURCEMODS\${MODDIR}" "$INSTDIR"
    IfErrors +1 success
      MessageBox MB_OK $(LINK_ERROR)
      DetailPrint "$(LINK_ERROR)"
      ${Failure}
    success:
  SectionEnd
!endif

; Create the uninstaller + basic uninstall information.
; This is the first post-uninstall task we do so people can uninstall e.g. a
; broken install. Additional version info is created after installation, in
; the section UPDATE_UNINSTALL_INFORMATION, so that the most recent version info
; is created (e.g. if the user aborts the update download or it fails, the
; actually installed version will be represented in the registry)
Section "" WRITE_UNINSTALLER
  IfFileExists "$SOURCEMODS\${MODDIR}_uninstall.exe" +1 InstallerDoesntAlreadyExist
    Delete "$SOURCEMODS\${MODDIR}_uninstall.exe"
  InstallerDoesntAlreadyExist:

  WriteUninstaller "$SOURCEMODS\${MODDIR}_uninstall.exe"

  ; For EstimatedSize: Take $GES_INSTALL_SIZE and multiply by 1024
  IntOp $0 $GES_INSTALL_SIZE * 1024

  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MODDIR}"

  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MODDIR}" \
                  "DisplayName" "${PROD_NAME}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MODDIR}" \
                  "UninstallString" "$SOURCEMODS\${MODDIR}_uninstall.exe"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MODDIR}" \
                  "InstallLocation" "$INSTDIR"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MODDIR}" \
                  "DisplayIcon" "${ICON_PATH}"
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MODDIR}" \
                  "EstimatedSize" "$0"
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MODDIR}" \
                  "NoModify" "1"
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MODDIR}" \
                  "NoRepair" "1"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${MODDIR}" \
                  "DisplayVersion" "$GES_UPDATE_VERSION"
SectionEnd

; nsisunz doesn't update the progress bar, so we show a marquee progress bar from this point onward
Section "" PROGRESSBAR_MARQUEE
  ; Hide main progress bar
  ShowWindow $prgInstfiles 0
  ; Show our custom one
  ShowWindow $prgInstfilesCustom 1

  ; Set our custom progress bar to marquee
  System::Call "user32::GetWindowLong(i $prgInstfilesCustom, i ${GWL_STYLE}) i .r1"
  System::Call "user32::SetWindowLong(i $prgInstfilesCustom, i ${GWL_STYLE}, i $1|${PBS_MARQUEE})"
  SendMessage $prgInstfilesCustom ${PBM_SETMARQUEE} 1 33
SectionEnd

!ifndef PATCH
  Section "" GES_INSTALL
    ${Status} $(INSTALLING)

    ; The full installer installs files from an archive distributed with the installer
    !ifdef FULL
      StrCpy $0 "$EXEDIR\${FILE_NAME} Files"
    !else
      StrCpy $0 $DOWNLOAD_FOLDER
    !endif

    DetailPrint "Attempting to extract $0\${INSTALL_ZIP_NAME}"
    !insertmacro unzip "$0\${INSTALL_ZIP_NAME}" $INSTDIR

    !ifndef FULL
      ; Delete the install file
      Delete "$0\${INSTALL_ZIP_NAME}"
    !endif

  SectionEnd
!endif

Section "" PATCH_INSTALL
  ${Status} $(INSTALLING)
  ; Delete phase -- delete files listed in removed.txt
  IfFileExists $PLUGINSDIR\PATCH\removed.txt DeleteFiles
  DetailPrint "$PLUGINSDIR\PATCH\removed.txt does not exist. No files will be deleted."
  Goto SkipDeleteFiles

  DeleteFiles:
  DetailPrint "Processing files to be deleted..."
  ; Open file for reading
  FileOpen $5 "$PLUGINSDIR\PATCH\removed.txt" r
  FileSeek $5 0
  ReadLoop:
    ClearErrors
    FileRead $5 $R0 ; Read line into $R0
    IfErrors FinishReadLoop
    ${Trim} $R0 $R0 ; Remove possible spaces or newlines -- fixes "NAME INVALID" error
    Delete "$INSTDIR\$R0" ; Delete the file on current line of removed.txt
    IfErrors +1 ReadLoop
      DetailPrint "Error deleting $INSTDIR\$R0"
    Goto ReadLoop
  FinishReadLoop:
  FileClose $5
  DetailPrint "File delete phase completed."

  SkipDeleteFiles:

  DetailPrint "Installing patch files..."
  CopyFiles "$PLUGINSDIR\PATCH\${MODDIR}\*.*" "$INSTDIR"
  DetailPrint "Removing patch temp files"
  RMDir /r "$PLUGINSDIR\PATCH"
SectionEnd

; Function to restore GE:S data we backed up
; We don't remove user data in the patch installer, so we don't include these sections
!ifndef PATCH
  Function RestoreData
    CreateDirectory "$SOURCEMODS\${MODDIR}"
    CreateDirectory "$SOURCEMODS\${MODDIR}\cfg"
    CopyFiles "$PLUGINSDIR\BACKUP\gamestate.txt" "$SOURCEMODS\${MODDIR}\gamestate.txt"
    CopyFiles "$PLUGINSDIR\BACKUP\gesdata.txt" "$SOURCEMODS\${MODDIR}\gesdata.txt"
    CopyFiles "$PLUGINSDIR\BACKUP\config.cfg" "$SOURCEMODS\${MODDIR}\cfg\config.cfg"
  FunctionEnd

  Section "" RESTORE_DATA
    Call RestoreData
  SectionEnd
!endif

!ifndef PATCH
  Section "" CREATE_SHORTCUT
    SetShellVarContext all
    Delete "$SMPROGRAMS\${FILE_NAME}.url"
    WriteINIStr "$SMPROGRAMS\${FILE_NAME}.url" "InternetShortcut" "URL" "steam://rungameid/${MODAPPID}"
    WriteINIStr "$SMPROGRAMS\${FILE_NAME}.url" "InternetShortcut" "IconIndex" 0
    WriteINIStr "$SMPROGRAMS\${FILE_NAME}.url" "InternetShortcut" "IconFile" "${ICON_PATH}"
    ; This creates a localization string that shows the full name, including the colon ':', in the Start menu
    WriteINIStr "$SMPROGRAMS\desktop.ini" "LocalizedFileNames" "${FILE_NAME}.url" "${PROD_NAME}"
  SectionEnd
!endif

!ifndef PATCH
  Section "" SET_DISPLAY_RESOLUTION
    ; This section is basically responsible for pre-setting the user's in-game resolution
    ; to their primary monitor's current display resolution, so that the game does not start
    ; at its default 800x600.

    ; To do so, we need to get the device ID of the graphics adapter since this is what the
    ; game uses to determine if the graphics hardware has changed. If this value is incorrect
    ; or unset, the game will reset to defaults.

    ; Here, we use PowerShell code to get the PNPDeviceID, which contains the vendor and device
    ; IDs.
    
    ; Multiple display adapters can exist on a single system, e.g. for hybrid graphics.

    ; We try to filter by active devices. If multiple devices are present, we try to filter out
    ; the Intel adapter (if any), and use the first remaining display adapter.

    DetailPrint "Setting default display resolution, if applicable..."

    nsExec::ExecToStack "\
      powershell.exe -c \
      $$controllers = Get-WmiObject Win32_VideoController | Where-Object { $$_.Availability -eq 3 };\
      \
      If ( ($$controllers | Measure).Count -gt 1 ) {\
          \
          $$controllersExclIntel = $$controllers | Where-Object { $$_.PNPDeviceID -NotLike '*VEN_8086*' };\
          \
          If ( ($$controllersExclIntel | Measure).Count -gt 0 ) {\
              $$controllers = $$controllersExclIntel;\
          }\
          \
      }\
      \
      Write-Host -NoNewline ($$controllers.PNPDeviceID | Select-Object -Index 0);\
      "
    Pop $0
    Pop $0

    ; Now we get the vendor and device IDs from the output string

    ; $0 is the PNPDeviceID output by PowerShell,
    ;    and is used for a temp var after the string has been parsed
    ; $1 is the accumulator used for our loops
    ; $2 is the string length of the PNPDeviceID,
    ;    and later is the output of ${Validate}
    ; $3 is the vendor ID
    ; $4 is the device ID
    ; $5 is the horizontal resolution
    ; $6 is the vertical resolution
    ; $7 is the user key under HKEY_USERS that we are currently processing

    DetailPrint "PNPDeviceID: $0"
    StrLen $2 $0

    StrCpy $1 0
    LookForVendorID:
      IntCmp $2 $1 ErrorParsingDeviceID
      StrCpy $3 $0 4 $1
      IntOp $1 $1 + 1
      StrCmp $3 "VEN_" 0 LookForVendorID
      IntOp $1 $1 + 3
      StrCpy $3 $0 4 $1
      DetailPrint "Vendor ID: $3"

    StrCpy $1 0
    LookForDeviceID:
      IntCmp $2 $1 ErrorParsingDeviceID
      StrCpy $4 $0 4 $1
      IntOp $1 $1 + 1
      StrCmp $4 "DEV_" 0 LookForDeviceID
      IntOp $1 $1 + 3
      StrCpy $4 $0 4 $1
      DetailPrint "Device ID: $4"

    ; Validate device and vendor IDs

    ${Validate} $2 $3 ${HEXADECIMAL}
    StrCmp $2 1 0 ErrorParsingDeviceID
    ${Validate} $2 $4 ${HEXADECIMAL}
    StrCmp $2 1 0 ErrorParsingDeviceID

    Goto ParseSuccess

    ErrorParsingDeviceID:
      DetailPrint "Error parsing device and vendor IDs. Not setting in-game resolution."
      Return
    ParseSuccess:

    System::Call 'user32::GetSystemMetrics(i 0) i .r5'
    System::Call 'user32::GetSystemMetrics(i 1) i .r6'
    DetailPrint "Detected resolution of the primary monitor: $5 x $6"

    ; Check if resolution is too high (larger than 2560 x 1600)
    ; High resolutions can impact stability, so we don't set them by default.
    IntCmp $5 2560 0 0 ResolutionError
    IntCmp $6 1600 0 0 ResolutionError
    Goto ResolutionOK

    ResolutionError:
      DetailPrint "Screen size of $5 x $6 is larger than one of 2560 x 1600. Not pre-setting resolution."
      Return

    ResolutionOK:

    ; Now enumerate through HKEY_USERS, setting the default display settings for each user.
    ; We don't set the display settings in two circumstances:
    ;   1) If HKEY_USERS\X\Software\Valve\* doesn't exist
    ;        We assume that they don't use steam, and don't need the settings.
    ;        This also protects against writing data in special user keys, like .DEFAULT, S-1-5-18, *_Classes, etc.
    ;        And also serves as a sanity check against other stuff
    ;   2) If the ${MODDIR} settings key already exists.

    DetailPrint "Enumerating HKEY_USERS registry keys"

    StrCpy $1 0
    EnumRegKeys:
      EnumRegKey $7 HKEY_USERS "" $1
      StrCmp $7 "" ExitLoop
      IntOp $1 $1 + 1

      ; Check if Software\Valve\* exists, and if not, skip this user
      EnumRegKey $0 "HKEY_USERS" "$7\Software\Valve" 0
      StrCmp $0 "" +1 ValveKeyExists
        DetailPrint "Skipping user $7: Key or subkeys not present: Software\Valve"
        Goto EnumRegKeys
      ValveKeyExists:

      ; Check if any values of the settings key exist, and if so, skip this user to avoid overwriting settings
      EnumRegValue $0 "HKEY_USERS" "$7\Software\Valve\Source\$SOURCEMODS\${MODDIR}\Settings" 0
      StrCmp $0 "" SettingsKeyDoesntExist
        DetailPrint "Skipping user $7: Settings key already exists: Software\Valve\Source\$SOURCEMODS\${MODDIR}\Settings"
        Goto EnumRegKeys
      SettingsKeyDoesntExist:

      WriteRegDWORD HKEY_USERS "$7\Software\Valve\Source\$SOURCEMODS\${MODDIR}\Settings" "VendorID" "0x$3"
      WriteRegDWORD HKEY_USERS "$7\Software\Valve\Source\$SOURCEMODS\${MODDIR}\Settings" "DeviceID" "0x$4"
      WriteRegDWORD HKEY_USERS "$7\Software\Valve\Source\$SOURCEMODS\${MODDIR}\Settings" "ScreenWidth" "$5"
      WriteRegDWORD HKEY_USERS "$7\Software\Valve\Source\$SOURCEMODS\${MODDIR}\Settings" "ScreenHeight" "$6"

      DetailPrint "Wrote registry settings for user key: $7"
      
      Goto EnumRegKeys

    ExitLoop:

    DetailPrint "Finished setting per-user display settings"

  SectionEnd
!endif

Section "" INSTALL_ENDED
  StrCpy $INSTALL_BEGAN ""
  DetailPrint "Install finished"
  Call SaveDetailsView
SectionEnd