; This function gets the free space of a path (in megabytes)
; and returns it to the stack.
; This requires "FileFunc.nsh"

; Push drive_letter | path
; Call getDiskFree
; Pop $OutVar

Function getDiskFree
  ; $0 = Free space of target directory, in megabytes (actually mebibytes)
  Push $0
  ; $1 = Directory to check
  Push $1
  Exch 2
  Pop $1
  Exch
  ; $2 = Accumulator for the recursive function below
  Push $2
  ; $3 = strlen of modified string
  Push $3
  ; $4 = scratch var
  Push $4

  ; This section checks if the directory exists.
  ; If not, it recursively adds "\.." until it finds one that does,
  ; with a max 128 recursions.
  ;
  ; The reason we do this is ${DriveSpace} doesn't work for dirs that don't
  ; yet exist.

  ; Initialize accumulator
  StrCpy $2 1
  ; Initialize strlen
  StrLen $3 $1

  StartLoop:
    IfFileExists "$1\*.*" EndLoop +1
      ; If we exceeded the max iterations, error out to avoid an infinite loop.
      IntCmp 128 $2 Error Error

      ; Subtract last char until we get \ or /
      StartCharDelete:
        IntOp $3 $3 - 1 ; subtract 1 from str length
        IntCmp $3 0 Error Error ; Error out if strlen <= 0
        StrCpy $1 $1 $3 ; make string match new str length
        StrCpy $4 $1 1 -1 ; copy last char of string to $4
        StrCmp $4 '/' EndCharDelete
        StrCmp $4 '\' EndCharDelete
        Goto StartCharDelete

      EndCharDelete:

      ; Subtract one more char from string, eliminate trailing slash
      IntOp $3 $3 - 1
      StrCpy $1 $1 $3

      IntOp $2 $2 + 1 ; add to accumulator

      Goto StartLoop

      Error:
        MessageBox MB_OK|MB_ICONEXCLAMATION "getDiskFree reached max iterations"
        StrCpy $0 0
        Goto Return

  EndLoop:

  ; get free space of $1 and write output to $0
  ${DriveSpace} "$1" "/D=F /S=M" $0

  Return:
  ; Revert vars to their previous states, leaving free space on the stack
  Pop $4
  Pop $3
  Pop $2
  Pop $1
  Exch $0
FunctionEnd
